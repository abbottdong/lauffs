# -*-coding:utf-8 -*-
from luffa.mobiler import appload, WaitElement
import unittest

class AppiumUnitSample(unittest.TestCase):

    def setUp(self):
        self.driver = appload().startapp()

    def tearDown(self):
        self.driver.quit()


    def test_lagou_login(self):
        # 智能等待登录页加载完成
        account = WaitElement(self.driver, lambda driver: self.driver.find_element_by_id(
            "com.alpha.lagouapk:id/et_four_guide_fragment_account"))
        if account:
            # 执行登录相关输入
            account.send_keys("d1988505j@163.com")
            pwd = self.driver.find_element_by_id("com.alpha.lagouapk:id/et_four_guide_fragment_pwd")
            pwd.send_keys("123456")
            submit = self.driver.find_element_by_id("com.alpha.lagouapk:id/login_button")
            submit.click()
            # 登录断言
            login_name = WaitElement(self.driver, lambda driver: self.driver.find_elements_by_id(
            "android:id/title"))
            self.assertIsNotNone(login_name,msg="登录失败")
            # 退出登录操作
            login_name[-1].click()
            self.driver.find_element_by_id("com.alpha.lagouapk:id/user_setting_layout").click()
            self.driver.find_element_by_id("com.alpha.lagouapk:id/log_txt").click()
            self.driver.find_element_by_id("com.alpha.lagouapk:id/dlg_ok").click()



if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(AppiumUnitSample)
    unittest.TextTestRunner(verbosity=2).run(suite)