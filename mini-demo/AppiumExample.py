# -*-coding:utf-8 -*-
from appium import webdriver
from selenium.webdriver.support.ui import WebDriverWait

desired_caps = {}
desired_caps['platformName'] = 'Android'
desired_caps['platformVersion'] = '5.0'
desired_caps['deviceName'] = 'X8QDU15210001536'
desired_caps['app'] = 'C:\\work\\code\python\\autoDemo\\data\\apk\\LagouApk-release.apk'  # apk路径
desired_caps['appPackage'] = 'com.alpha.lagouapk'  # app程序包名
desired_caps['appActivity'] = 'com.alpha.lagouapk.HelloActivity'   # 需要启动的Activity
desired_caps['unicodeKeyboard'] = True  # 中文输入设置
desired_caps['resetKeyboard'] = True
driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)
account = WebDriverWait(driver, timeout=10).until(lambda dirver: driver.find_element_by_id(
    "com.alpha.lagouapk:id/et_four_guide_fragment_account"))
pwd = WebDriverWait(driver, timeout=50).until(lambda dirver: driver.find_element_by_id(
    "com.alpha.lagouapk:id/et_four_guide_fragment_pwd"))
account.send_keys('d1988505j@163.com')
pwd.send_keys('123456')
driver.find_element_by_id('com.alpha.lagouapk:id/login_button').click()
driver.quit()