# -*-coding:utf-8 -*-
from luffa.mobiler import appload, WaitElement
import unittest

class ElementSample(unittest.TestCase):

    def setUp(self):
        self.driver = appload().startapp()

    def tearDown(self):
        self.driver.quit()

    def test_lagou_login(self):
        # 智能等待登录页加载完成
        account = WaitElement(self.driver, lambda driver: self.driver.find_element_by_id(
            "com.alpha.lagouapk:id/et_four_guide_fragment_account"))  # ID定位
        if account:
            # 执行登录相关输入
            account.send_keys("d1988505j@163.com")
            pwd.send_keys("123456")



if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(ElementSample)
    unittest.TextTestRunner(verbosity=2).run(suite)