# -*-coding:utf-8 -*-

import requests
from requests import RequestException
from luffa.common import logs
import json
import settings

if settings.SSL=='on':
    domain = "https://" + settings.URL
else:
    domain = "http://" + settings.URL


class SendHttpRequest(object):
    """
        http请求方法封装目前，封装的主要方法为发送json post请求和普通url get请求
    """
    def __init__(self, URI, url=domain,  headers=None, files=None):
        """
        :param URI:请求的URI路径
        :param url:请求的服务器地址
        :param headers:请求的头部
        :param files:请求文件
        :return:
        """
        self.url = url + "/" + URI
        self.headers = headers
        self.files = files

    def post(self,value=None, json_str=True):
        """
        :param value:提交的POST body数据
        :return: json返回值
        """
        if settings.ENCRYPT==True:
            encrypt = settings.ENCRYPTMODE
            if encrypt == 'AES':
                from luffa.common import prpcrypt
                try:
                    data = prpcrypt(settings.AESKEY, settings.AESIV).encrypt(value)
                    logs().debug(u"数据%s通过AES加密结果为：\n %s" %(value, data))
                except Exception, b:
                    logs().error(b)
            elif encrypt == 'BASE64':
                print "BASE64 do something"
        else:
            if json_str  == True:
                data = json.dumps(value)
            else:
                data =value
        if data:
            try:
                req = requests.post(self.url, data=data, headers=self.headers, files=self.files, verify=False, allow_redirects=True)
                print req.url
                if req.status_code == 200:
                    logs().debug(u'请求信息：请求地址：%s,\n 请求参数：%s,\n请求header:%s,\n'
                                     u'请求内容：%s' %(req.url, value, req.headers, req.text))
                    try:
                        req_json = req.json()
                        if req_json:
                            return req_json
                    except Exception, e:
                        logs().warning(u'发送请求%s 请求地参数%s, 返回数据不是json' %(req.url, value))
                        return req.text
                else:
                    logs().error(u'服务器返回%s %s' %(req.status_code, req.text))
            except RequestException, err:
                logs().error(u'请求发生错误：%s' % err)


    def get(self):
        """
        :param value:提交的POST body数据
        :return: json返回值
        """
        try:
            req = requests.get(self.url, headers=self.headers)
            if req.status_code == 200:
                logs().debug(u'请求信息：[请求地址：%s, 请求参数：%s,请求header:%s] \n'
                                 u'返回结果：----------------body----------------------\n'
                                 u'%s\n'
                                 u'-------------------------body----------------------\n' %(req.url, req.headers, req.text))
                try:
                    req_json = req.json()
                    if req_json:
                        return req_json
                except Exception, e:
                    logs().warning(u'发送请求%s 请求地参数%s, 返回数据不是json' %(req.url))
                    return req.text
        except RequestException, err:
            logs().error(u'请求发生错误：%s' % err)