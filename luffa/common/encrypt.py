# -*-coding:utf-8 -*-

#*************************************************
#AES加解密 RSA加密
#*************************************************
from Crypto.Cipher import AES
import base64
from binascii import a2b_hex
import requests
import settings

class prpcrypt(object):

    def __init__(self, key, iv):
        '''
        :param key:AES　加密key
        :return:
        '''
        self.key = key
        self.iv = iv
        BS = AES.block_size
        self.pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)
        self.unpad = lambda s : s[0:-ord(s[-1])]

    def encrypt(self, text):
        """
        :param text:需要加密的字符串
        :return: 加密后的base64编码字符串
        """

        raw = self.pad(text)
        cipher = AES.new( self.key, AES.MODE_CBC, self.iv )
        hex_data =  cipher.encrypt(raw).encode("hex")
        return base64.b64encode(a2b_hex(hex_data))

    def decrypt(self,text):
        """
        :param text: 需要解密的字符串
        :return: 解密后的base64编码字符串
        """
        enc = base64.decodestring(text)
        cipher = AES.new(self.key, AES.MODE_CBC, self.iv)
        return self.unpad(cipher.decrypt(enc))