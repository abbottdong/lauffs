#-*-coding:utf-8 -*-
import MySQLdb
from MySQLdb import MySQLError
import settings
from luffa_log import logs


# 连接数据库执行sql返回值
class ExecSQL():
    def __init__(self, host, port, user, password, database):
        '''
        :param host:数据库地址
        :param port:数据库端口
        :param user:数据库用户名
        :param password:数据库密码
        :param database:数据库名
        :return:
        '''
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.database = database

    #执行MYSQL查询
    def exec_mysql(self, sql):
        '''
        :param sql:要执行的SQL语句
        :return:
        '''
        try:
            conn = MySQLdb.connect(host=self.host,
                                   port=self.port,
                                   user=self.user,
                                   passwd=self.password,
                                   db=self.database,
                                   charset="utf8")
            cur = conn.cursor()
            if cur:
                logs().debug(u"执行SQL语句|%s|" %sql)
                cur.execute(sql)
                rows = cur.fetchall()
                cur.close()
                conn.commit()
                if len(rows) == 0:
                    logs().warning(u"没有查询到数据")
                return rows
            else:
                logs().error(u"数据库连接不成功")
        except MySQLError, e:
            logs().error(e)
        finally:
            if conn:
                conn.close()



def runSql(sql):
    db_class = settings.DATABASE.get('ENGINE')
    host = settings.DATABASE.get('HOST')
    port = settings.DATABASE.get('PORT')
    user = settings.DATABASE.get('USER')
    password = settings.DATABASE.get('PWD')
    database = settings.DATABASE.get('DATABASE')
    if db_class == 'MYSQL':
        sql_result = ExecSQL(host,
                             port,
                             user,
                             password,
                             database).exec_mysql(sql)
        return sql_result
    #elif db_class == 'MSSQL':  do something
    else:
        logs().error(u"数据库配置名称不正确！")

"""
a = runSql("select verify_code from verify_code where memo=13464104158;")
print a
"""
