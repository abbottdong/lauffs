# -*-coding:utf-8 -*-
# !/usr/bin/python
__author__ = 'abbott'

import json
import xmltodict
import random
import settings
import time
import urllib
import sys
import re
reload(sys)
sys.setdefaultencoding("utf-8")


class jsonprase(object):
    """
        解析json数据和转换json数据
    """
    def __init__(self, json_value):
        self.json_value = json_value

    def find_json_node_by_xpath(self, xpath):
        '''
        :param xpath: 简单的xpath结构如/city/city_id
        :return: path下的数据，可能是字符串或是列表
        '''
        elem = self.json_value
        nodes = xpath.strip("/").split("/")
        for x in range(len(nodes)):
            try:
                elem = elem.get(nodes[x])
            except AttributeError:
                elem = [y.get(nodes[x]) for y in elem]
        return elem

    def datalength(self, xpath="/"):
        '''
        :param xpath:简单的xpath结构如/city/city_id
        :return: 数据长度
        '''
        return len(self.find_json_node_by_xpath(xpath))

    @property
    def json_to_xml(self):
        try:
            root = {"root": self.json_value}
            xml = xmltodict.unparse(root, pretty=True)
        except ArithmeticError, e:
            print e
        return xml

# 解析xml字符串
class xmlprase(object):
    def __init__(self, xml_value):
        self.xml_str = xml_value

    @property
    def xml_to_json(self):
        try:
            xml_dic = xmltodict.parse(self.xml_str,
                                      encoding="utf-8",
                                      process_namespaces=True,
                                      )
            json_str = json.dumps(xml_dic)
        except Exception, e:
            print e
        return json_str


class dataDriver(object):
    """
        常用的构造数据的方法集
    """
    def phoneNumber(self, number_src, num=1, files=None):
        """
        :param number_src:号码开头，如134
        :param files: 需要存储的文件
        :return:
        """
        number_list = []
        for x in range(num):
            number_r = ""
            for x in range(0,8):
                n = random.choice(range(0,10))
                number_r += str(n)
            number = str(number_src)+number_r
            number_list.append(number)
        if files is not None:
            with open(files, 'a') as f:
                f.write('\n'.join(number_list))
            f.close()
        else:
            return number_list

    #生成参数数据字典
    def argsDict(self, dicts=None, version=settings.APPVERSION, client='ad'):
        if client=='ad':
            ag_default = {"imei":"865473026034143",
                          "version":version,
                          "opSource":client
            }
        elif client=='ios':
            ag_default = {"imei":"23ffgffffffffffffffff",
                          "version":version,
                          "opSource":client,}
        else:
            raise Exception(u"args client is 'ad' or 'ios'")
        if dicts:
            if isinstance(dicts, dict):
                nd = dict([(k,urllib.quote(str(v))) for (k,v) in dicts.items()])
                data = dict(nd.items()+ag_default.items())
                return json.dumps(data)
        else:
            return json.dumps(ag_default)

    #随机生成18位身份证号码
    def identity(self):
        ARR = (7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2)
        LAST = ('1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2')
        t = time.localtime()[0]
        x = '%02d%02d%02d%04d%02d%02d%03d' %(random.randint(10,99),
                                            random.randint(01,99),
                                            random.randint(01,99),
                                            random.randint(t - 80, t - 19),
                                            random.randint(1,12),
                                            random.randint(1,28),
                                            random.randint(1,999))
        y = 0
        for i in range(17):
            y += int(x[i]) * ARR[i]
        return '%s%s' %(x, LAST[y % 11])

    #随机中文
    def chinese(self, num=1):
        ch = ""
        for x in range(0, num):
            head = random.randint(0xB0, 0xCF)
            body = random.randint(0xA, 0xF)
            tail = random.randint(0, 0xF)
            val = ( head << 8 ) | (body << 4) | tail
            str ="%x"% val
            try:
                st = str.decode('hex').decode('gb2312')
                if st:
                    ch += st
            except Exception, e:
                pass
        return ch

    # 随机生成银行卡号
    @property
    def cardNum(self):
        card = random.randrange(0, 9999999999999999, 16)
        return str(card)




class caseDataLoader():
    def __init__(self, path):
        '''
        :param path:测试文件路径
        :return:
        '''

        self.path = path

    def _loadFile(self):
        '''
        :return:测试数据文件中的json字符串
        '''
        with file(r'%s' % self.path) as f:
            js = json.load(f, encoding='utf-8')
        f.close()
        return js

    @property
    def get_case_name(self):
        '''
        :return: 测试用例名称
        '''
        case_name = settings.CASE_MODULE.get("caseName")
        return self._loadFile().get(case_name)

    @property
    def get_case_url(self):
        '''
        :return:测试用例url地址
        '''
        case_url = settings.CASE_MODULE.get('caseUrl')
        urls = self._loadFile().get(case_url)
        return urls

    @property
    def get_case_data(self):
        '''
        :return:测试用例数据
        '''
        data = settings.CASE_MODULE.get("data")
        arg_order = settings.CASE_MODULE.get('order')
        data_array = []
        for case in self._loadFile().get(data):
            for k,v in case.items():
                    reg = re.match(r"^\{{(.*?)\}}$", v)
                    if reg:
                        case[k] = eval(reg.groups()[0])
            t = tuple([case[y].decode('utf-8') for y in self._loadFile().get(arg_order)])
            data_array.append(t)
        return data_array
