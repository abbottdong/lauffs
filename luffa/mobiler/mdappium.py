# -*-coding:utf-8
from urllib2 import URLError
import settings
from appium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException, WebDriverException
from luffa.common import logs
from ApkRead import apkmess
from mobilehard import mobilehard
from ADBShell import devices, AdbCommand
import re


class SettingsConfException(Exception):
    def __init__(self, msg):
        Exception.__init__(self)
        self.msg = msg

#检查配置文件需要用的配置是否存在
try:
    settings.APK_PATH
    settings.APPIUMSERVER
    settings.ELEMENTTIMEOUT
except AttributeError, e:
    raise SettingsConfException(u"配置文件出错！未找到配置参数APK_PATH， APPIUMSERVER， ELEMENTTIMEOUT")


class appload:
    """
        封装备appium的启动程序，从测试包和系统读取相关配置启动被测试app
    """
    def __init__(self):
        self.device = devices()

    def startapp(self, apk=None, activity=None):
        """
        :param apk: 测试apk路径
        :param activity: 测试app和activity
        :return: driver
        """
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = mobilehard(self.device).mobile_android_version
        if activity is None:
            activity = apkmess(settings.APK_PATH).launchable_activity
        apk_path = settings.APK_PATH
        desired_caps['deviceName'] = self.device
        app_info = apkmess(apk_path)
        desired_caps['app'] = apk
        desired_caps['appPackage'] = app_info.package_name
        desired_caps['appActivity'] = activity
        desired_caps['unicodeKeyboard'] = True
        desired_caps['resetKeyboard'] = True
        print desired_caps
        try:
            logs().debug(u'appium启动信息 %s' % desired_caps)
            driver = webdriver.Remote(settings.APPIUMSERVER, desired_caps)
        except URLError, e:
            logs().error(u'请检果appium服务器地址是否正确! \t %s ' % e)
        except WebDriverException, err:
            logs().error(u'appium服务器session被占用!, \t %s' % err)
        return driver

    def coverinstall(self, apkpath = settings.APK_PATH):
        '''
        :param apkpath: 需要被安装的app的路径，默认为配置文件路径
        :return:Ture or False
        '''
        ins_command = AdbCommand(self.device).shell.get("coverInd") + ' ' + apkpath
        run = mobilehard(self.device).execadbshell(ins_command)
        if run:
            result = [x for x in run if x!='\n'][1]
            if re.match(r"^Failure*", result):
                logs().error(u"此版本不能覆盖比自己高的版本！")
                return False
            elif re.match(r"^Success*", result):
                return True
            else:
                logs().error(u"adb命令运行有误！")
                raise Exception(u"adb命令运行有误！")
        else:
            logs().error(u"adb命令运行有误!")
            raise Exception(u"adb命令运行有误！")



def WaitElement(webDriver, method, timeout=settings.ELEMENTTIMEOUT):
    """
    set find elements auto wait
    :param webDriver: the start webdriver
    :param method: the method of check page element
    :param timeout: Timeout
    :return: element
     Example:
            from appo.specialUi import WaitElement \n
            element = WaitElement(driver, lambda x: x.find_element_by_id("someId"), 10)
    """
    try:
        elements = WebDriverWait(webDriver, timeout).until(method)
        return elements
    except TimeoutException, e:
        logs().error(u"页面加载超时 [%s] %s" %(method, e))

