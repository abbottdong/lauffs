# -*-coding:utf-8 -*-
import os
from luffa.common import logs
import settings





class apkmess:
    """
    apk_path: 获取测试app相关的信息
    """

    def __init__(self, apk_path):
        """
        :param apk_path: 测试pak路径
        :return:
        """
        self.apk_path = apk_path

    def _get_apk_info_all(self):
        if os.path.exists(self.apk_path):
            cmd_command = "aapt dump badging %s" % settings.APK_PATH
            apk_info = os.popen(cmd_command)
            info_dict = {}
            for one_info in apk_info:
                info = one_info.split(": ")
                if len(info) == 2:
                    val_key = info[0]
                    val_value = info[1]
                    info_dict[val_key] = val_value
            return info_dict
        else:
            raise Exception(u"测试apk路径不存在！")

    def _get_package_info(self, lab):
        package_info = self._get_apk_info_all().get(lab)
        all_info = package_info.split(" ")
        dict_info = {}
        for one in all_info:
            the_info = one.split("=")
            if len(the_info) == 2:
                info_key = the_info[0]
                info_value = the_info[1]
                dict_info[info_key] = info_value
        return dict_info


    @property
    def apkSize(self):
        """
        :return: 获取APK的大小
        """
        size =  os.path.getsize(self.apk_path)/(1024.0*1024.0)
        return "%.2f" % size

    @property
    def package_name(self):
        """
        :return:获取测试包的包名
        """
        return self._get_package_info("package").get("name").strip("'")

    @property
    def version_code(self):
        """
        :return:测试包的版本号
        """
        return self._get_package_info("package").get("versionCode").strip("'")

    @property
    def version_name(self):
        """
        :return:获取测试包的版本名称
        """
        return self._get_package_info("package").get("versionName").strip("\n").strip("'")


    @property
    def application_label(self):
        """
        :return:获取应用名称
        """
        label_info = self._get_package_info("launchable-activity")
        return label_info.get("label").strip("'")


    @property
    def launchable_activity(self):
        """
        :return:获取app的入口activity名称
        """
        activity_info = self._get_package_info("launchable-activity")
        return activity_info.get("name").strip("'")