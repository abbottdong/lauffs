#-*-coding:utf-8 -*-
__author__ = 'DongJe'

import time
import subprocess
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from luffa.common import logs
from mdappium import appload


class UIHandle(object):
    def __init__(self):
        self.driver = appload().startActivity()

    def passFirstLoad(self):
        try:
            main_elements = WebDriverWait(self.driver, 10).until(
                lambda driver: self.driver.find_element_by_id(
                    "com.topview.slidemenuframe:id/load_up"))
            if main_elements:
                x = self.driver.get_window_size()['width']
                y = self.driver.get_window_size()['height']
                x_zuobiao = float(x) / float(2)
                y_start = float(y) * 4 / float(5)
                y_stop = float(y) / float(5)
                for m in range(2):
                    self.driver.swipe(x_zuobiao, y_start, x_zuobiao, y_stop)
                    time.sleep(1)
                element = WebDriverWait(self.driver, 5).until(
                    lambda driver: self.driver.find_element_by_class_name(
                        "android.widget.ImageButton"))
                element.click()
        except TimeoutException, e:
            pass
        return self.driver


def WxPassword(x, y, password):
    '''
    :param x: 横轴坐标
    :param y: 坚轴坐标
    :return: 0-9数字坐标
    '''
    a, b, c = x/(3.0*2), x/2.0, x-(x/(3.0*2))
    e, f, g, h = y*9.0/10.0, y*7.5/10.0, y*8.5/10.0,y*6.5/10.0
    coordinate = {0: (b,e),
                  1: (a,h),
                  2: (b,h),
                  3: (c,h),
                  4: (a,f),
                  5: (b,f),
                  6: (c,f),
                  7: (a,g),
                  8: (b,g),
                  9: (c,g)}
    coors = [coordinate[int(x)] for x in list(password)]
    return coors


def coverInstall(apkPath):
    """
    install phoneOptions of cover the mobile already exists
    :param apkPath: apk path of need to install
    :return:
    """
    command = "adb install -r %s" % apkPath
    p = subprocess.Popen(command,
                         universal_newlines=True,
                         stdout=subprocess.PIPE,
                         stdin=subprocess.PIPE,
                         stderr=subprocess.STDOUT,
                         shell=True,)
    while True:
        if p.poll() == 0:
            break
        elif p.poll() == -1:
            logs().error('exec adb shell 【%s】 the device not found or mobile can not be shell' % command)
            break
        else:
            time.sleep(0.5)
    rData = p.stdout.readlines()
    logs().debug('exec adb shell 【%s】 return %s' % (command, rData))
    return rData