#-*-coding:utf-8 -*-
__author__ = 'abbott'
from luffa.common import logs
import os


class AdbCommand(object):
    """
      常用ADB命令存放
    """
    def __init__(self, deviceNname):
        self.deviceNname = deviceNname
        self.shell = {
            #get the all devices name there mobile connect the computer
            'AllDevices':'adb devices',
            #get detailed information of the designated mobile phone
            'GetProp': 'adb -s %s shell getprop' % self.deviceNname,
            #get detailed cpu info of the designated mobile phone
            'getCpu' : 'adb -s %s shell cat /proc/cpuinfo' % self.deviceNname,
            #get detailed memory info of the designated mobile phone
            'getMem' : 'adb -s %s shell cat /proc/meminfo' % self.deviceNname,
            #install apk cover the old
            'coverInd' : 'adb -s %s install -r' % self.deviceNname,
        }

def devices():
    """
    :return:# 获取连接电脑的手机驱动编号, 成功连接手机的可用驱动编号
    """
    devices_command = "adb devices"
    command_out = os.popen(devices_command).readlines()
    if len(command_out) >= 3:
        devices_all = command_out[1:-1]
        print devices_all
        if len(devices_all)>1:
            print u"您的电脑上有多个活动的连接，将默认选择第一个连接进行测试！"
            logs().warning(u"您的电脑上有多个活动的连接，将默认选择%s这个连接进行测试" % devices_all[0])
        device_line = devices_all[0].strip("\n").split("\t")
        if len(device_line) == 2:
            device_name = device_line[0]
            device_status = device_line[1].strip("\n")
            if device_status == "device":
                return device_name
            else:
                logs().error(u"连接编号为%s的手机未获得与电脑连接的权限！" % device_name)
                raise Exception(u"连接编号为%s的手机未获得与电脑连接的权限！" % device_name)
        #费弃多个连接的列表返回改成单devices返回
        '''
        for device_info in devices_all:
            device = device_info.strip("\n").split("\t")
            if len(device) == 2:
                device_name = device[0]
                device_status = device[1].strip("\n")
                if device_status == "device":
                    devices.append(device_name)
                else:
                    appiumautolog().error(u"连接编号为%s的手机未获得与电脑连接的权限！" % device_name)
                    raise Exception(u"连接编号为%s的手机未获得与电脑连接的权限！" % device_name)
        '''
    else:
        logs().error(u"系统中adb命令不能正常使用或手机未正常连接！")
        raise AttributeError(u"系统中adb命令不能正常使用或手机未正常连接！")