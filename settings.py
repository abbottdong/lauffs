#-*-coding:utf-8 -*-
__author__ = 'abbott'
__data__ = '2014-6-10'

# ************************************************
#   配置系统相关的参数,提供全局的相关配置
# ************************************************
import os
BASE_DIR = os.path.dirname(__file__)

# 日志路径配置，以下配置为项目根目录下的log文件目录
LOGPATH = os.path.join(BASE_DIR, 'logs',).replace('\\', '/')
# 日志输出等级配置 log等级,1:notset 2:debug  3:info 4:warning 5:error 6:critical
LOGLEVEL = 2
# 测试数据路径配置
DATAPATH = os.path.join(BASE_DIR, 'data',).replace('\\', '/')

# 注册测试目录
CASE_SUITE = (
    'case.account_apis',
    'case.account_ui',
)


# ************************************************
# http接口测试相关配置
# ************************************************
# 测试域名
URL = "app.zallgo.com"
# 是否开启SSL 开启SSL为on;关才闭测为off
SSL = 'off'
# 证书路径
SSL_CER = os.path.join(BASE_DIR, 'root.cer')
# http参数是否需要加密,需要加密为True,不需要加密为False
ENCRYPT = False
# 加密方式，目前只支持了AES，如果是其它的需要在common.encrypt中扩展
ENCRYPTMODE = 'AES'
# AES加密配置
AESKEY = "1234567891234567"
AESIV= "0000000000000000"
# 默认的登录地址
#LOGINURL = 'http://192.168.8.135:8080/app_service/user/v1/userLogin'
LOGINURL = 'http://app.zallgo.com/api/user/login.json'


# ************************************************
# android UI测试相关配置
# ************************************************
#配置Android页面超时时间
ELEMENTTIMEOUT = 20
#配置appium服务器地址
APPIUMSERVER = "http://127.0.0.1:4723/wd/hub"
"""
#配置aapt工具
AAPT_TOOLS = os.path.join(BASE_DIR, 'tools','aapt.exe')

#配置adb工具
ADB_TOOLS = os.path.join(BASE_DIR, 'tools','adb.exe')
"""
#配置测试apk路径
APK_PATH = os.path.join(BASE_DIR, 'data', 'apk', 'LagouApk-release.apk')

# App版本配置
APPVERSION = '1.1.4'

# 数据库配置，支持MYSQL、MSSQL、ORACLE
DATABASE = {
    "ENGINE": "MYSQL",
    "HOST": "127.0.0.1",
    "PORT": 3307,
    "USER": "root",
    "PWD": "123456",
    "DATABASE": "p2pv4"
}

# 测试数据文件配置（json模板）
CASE_MODULE = {
    "caseName": "caseName",  # 测试用例json文件中用例名称配置名称
    "caseUrl": "reqURL",  # 测试用例对应该的URI地址
    "data": "data",  # 子用例数据配置名称
    "order": "order"  # 变量名配置和排序
}