# README #
###1.项目介绍：这是一个用python编写小型自动化测试框架支持http接口测试与基于appium的android ui自动化测试。
###2.目录介绍
#####├─case                 case业务层
#####│  ├─account_apis
#####│  └─account_ui
#####├─data                 case数据层
#####│  ├─account_apis
#####│  └─apk
#####├─logs                 调试debug信息
#####├─luffa                框架库
#####│  ├─common            公用方法
#####│  ├─httper            HTTP请求方法酷
#####│  └─mobiler           手机测试方法库
#####├─mini-demo            示例
#####├─results              测试结果
#####└─tools                其它辅助工具
#####settings.py            测试基础配置信息
#####run.py                 测试运行控制器

### 依赖库 ###

* requests
* appium-python-client(https://pypi.python.org/pypi/Appium-Python-Client)
* python-mysqldb(https://pypi.python.org/pypi/MySQL-python/1.2.5)
* HTMLTestRunner(https://pypi.python.org/pypi/HTMLTestRunner)

### 工具安装 ###

* appium.exe(https://bitbucket.org/appium/appium.app/downloads/)
* android-sdk(https://bitbucket.org/appium/appium.app/downloads/)


### 联系我 ###

* d1988505j@163.com
* QQ@:690455420