#-*-coding:utf-8 -*-
import subprocess
import settings
import os
from luffa.common import HTMLTestRunner
import unittest
import time


if __name__ == "__main__":
    testmodules = settings.CASE_SUITE
    suite = unittest.TestSuite()
    for t in testmodules:
        try:
            mod = __import__(t, globals(), locals(), ['suite'])
            print mod
            suitefn = getattr(mod, 'suite')
            print suitefn
            suite.addTest(suitefn())
        except (ImportError, AttributeError):
            suite.addTest(unittest.defaultTestLoader.loadTestsFromName(t))
    report_file = os.path.join(
        os.getcwd() + "\\results\\%s.html" % time.strftime("%Y_%m_%d_%H_%M_%S"))
    fp = file(report_file, 'wb')
    runner = HTMLTestRunner(
        stream=fp, title=u'测试报告', description=u'-----测试报告')
    result = runner.run(suite)
    fp.close()