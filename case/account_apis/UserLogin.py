# -*-coding:utf-8 -*-
from luffa.httper import SendHttpRequest
from luffa.common import jsonprase, dataDriver, caseDataLoader
import unittest
from ddt import data, ddt, unpack

@ddt
class UserLogin(unittest.TestCase):

    d = caseDataLoader("C:\\work\\code\\python\\autoDemo\\data\\account_apis\\UserLogin.json")
    case_url = d.get_case_url
    case_data = d.get_case_data
    #手机号登录和平台帐号登录
    @data(*case_data)
    @unpack
    def test_user_login(self, account, password, sysch, sysci, syscv, sysiv, syssy):
        headers = {"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"}
        data_str = {"sy_ch":sysch,
                    "sy_ci":sysci,
                    "sy_cv":syscv,
                    'uc_ua':account,
                    'uc_pa':password,
                    'sy_iv':sysiv,
                    'sy_sy':syssy}
        login = SendHttpRequest(self.case_url[0], headers=headers).post(data_str, json_str=False)
        print login
        code = jsonprase(login).find_json_node_by_xpath('/status')
        self.assertEqual(code, 1)
        token = jsonprase(login).find_json_node_by_xpath('/data/token')
        print token
        self.assertIsNotNone(token)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(UserLogin)
    unittest.TextTestRunner(verbosity=2).run(suite)
